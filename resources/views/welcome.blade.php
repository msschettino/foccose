@extends('layouts.site')

@section('content')
<!--<section id="slider" class="slider-parallax swiper_wrapper clearfix">

    <div class="swiper-container swiper-parent">
        <div class="swiper-wrapper">
            <div class="swiper-slide dark" style="background-image: url('images/slider/swiper/1.jpg');">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <h2 data-caption-animate="fadeInUp">Welcome to Canvas</h2>
                        <p data-caption-animate="fadeInUp" data-caption-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply
                            put them on our Canvas.</p>
                    </div>
                </div>
            </div>
            <div class="swiper-slide dark">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center">
                        <h2 data-caption-animate="fadeInUp">Beautifully Flexible</h2>
                        <p data-caption-animate="fadeInUp" data-caption-delay="200">Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality
                            that can be adapted to any screen size.</p>
                    </div>
                </div>
                <div class="video-wrap">
                    <video poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
                        <source src='images/videos/explore.mp4' type='video/mp4' />
                        <source src='images/videos/explore.webm' type='video/webm' />
                    </video>
                    <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
                </div>
            </div>
            <div class="swiper-slide" style="background-image: url('images/slider/swiper/3.jpg'); background-position: center top;">
                <div class="container clearfix">
                    <div class="slider-caption">
                        <h2 data-caption-animate="fadeInUp">Great Performance</h2>
                        <p data-caption-animate="fadeInUp" data-caption-delay="200">You'll be surprised to see the Final Results of your Creation &amp; would crave for more.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
        <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
        <div id="slide-number">
            <div id="slide-number-current"></div><span>/</span>
            <div id="slide-number-total"></div>
        </div>
    </div>

</section>-->

<!-- Content
		============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-top:40px;">

        <!--<a class="button button-full button-purple center tright header-stick bottommargin-lg">
            <div class="container clearfix">
                Canvas comes with Unlimited Customizations &amp; Options. <strong>Check Out</strong> <i class="icon-caret-right"
                    style="top:4px;"></i>
            </div>
        </a>-->

        <div class="container clearfix">


            <!-- Post Content
					============================================= -->
            <div class="postcontent nobottommargin clearfix">

                <!-- Posts
						============================================= -->
                <div id="posts" class="small-thumbs">

                    @if (isset($noticias) && count($noticias) > 0)
                    @foreach($noticias as $noticia)

                    <div class="entry clearfix">
                        <div class="entry-image">
                            <a href="uploads/{{$noticia->image}}" data-lightbox="image"><img class="image_fade" src="uploads/{{$noticia->image}}" alt="{{$noticia->titulo}}"></a>
                        </div>
                        <div class="entry-c">
                            <div class="entry-title">
                                <h2><a href="/noticia/{{$noticia->id}}">{{$noticia->titulo}}</a></h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i> {{$noticia->created_at->format('d/m/Y')}}</li>
                            </ul>
                            <div class="entry-content">
                                <p>{{$noticia->intro}}</p>
                                <a href="/noticia/{{$noticia->id}}" class="more-link">Leia Mais</a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <!-- #posts end -->

                <!-- Pagination
						============================================= -->
                <div class="pager nomargin">
                    {{ $noticias->links() }}
                </div>
                @endif
                <!-- .pager end -->

            </div>
            <!-- .postcontent end -->

            <!-- Sidebar
					============================================= -->
            <div class="sidebar nobottommargin col_last clearfix">
                <div class="row">
                <div class="fb-page" data-href="https://www.facebook.com/foccosergipe" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/foccosergipe" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/foccosergipe">Focco Sergipe</a></blockquote></div>
                </div>
                <div class="row" style="margin-top:20px;padding:10px;">
                    <div class="col-md-12"><a href="{{url('/conteudo/denuncie')}}"><img src="{{asset('images/denuncie.jpg')}}"></a></div>
                </div>
            </div>
            <!-- .sidebar end -->

        </div>

    </div>

</section>
<!-- #content end -->

@endsection