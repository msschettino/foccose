<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
		rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/swiper.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />

	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- Document Title
	============================================= -->
	<title>Focco - Fórum de Combate à Corrupção de Sergipe</title>

</head>


<body class="" style="background-image: url('{{asset('images/pattern.png')}}'); background-attachment: fixed;">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=109934229168702";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!--<body class="stretched">-->

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">

				<!-- Logo
				============================================= -->
				<div id="logo" class="divcenter">
					<a href="/" class="standard-logo" data-dark-logo="{{asset('images/logo-dark.png')}}"><img class="divcenter" src="{{asset('images/logo.png')}}" alt="Canvas Logo" style="height:100px;margin-bottom:20px;padding-top:20px;"></a>
					<a href="/" class="retina-logo" data-dark-logo="{{asset('images/logo-dark@2x.png')}}"><img class="divcenter" src="{{asset('images/logo@2x.png')}}" alt="Canvas Logo"></a>
				</div>
				<!-- #logo end -->

			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2 center">

					<div class="container clearfix">

						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

						<ul>
							<li class="{{Request::is('conteudo/sobre') ? 'current' : ''}}">
								<a href="/conteudo/sobre">
									<div>Sobre</div>
								</a>
							</li>
							<li class="{{Request::is('/') ? 'current' : ''}}">
								<a href="/">
									<div>Notícias</div>
								</a>
							</li>
							<li class="{{Request::is('conteudo/fiscalizacao') ? 'current' : ''}}">
								<a href="/conteudo/fiscalizacao">
									<div>Transparência</div>
								</a>
							</li>
							<li class="{{Request::is('conteudo/denuncie') ? 'current' : ''}}">
								<a href="/conteudo/denuncie">
									<div>Denuncie</div>
								</a>
							</li>
							<li class="{{Request::is('conteudo/documentos') ? 'current' : ''}}">
								<a href="/conteudo/documentos">
									<div>Documentos</div>
								</a>
							</li>
							<li class="{{Request::is('contato') ? 'current' : ''}}">
								<a href="/contato">
									<div>Contato</div>
								</a>
							</li>
						</ul>

						<!-- Top Search
						============================================= -->
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="Digite &amp; Pressione Enter..">
							</form>
						</div>
						<!-- #top-search end -->

					</div>

				</nav>
				<!-- #primary-menu end -->

			</div>

		</header>
		<!-- #header end -->

		@yield('content')

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark" style="background: url('{{asset('images/footer-bg.jpg')}}') repeat; background-size: cover;">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix" style="padding:40px;">

					<div class="col_two_third">

						<div class="widget clearfix">

							<img width="130px" src="{{asset('images/logo-widget.png')}}" alt="" class="alignleft" style="margin-top: 8px; padding-right: 18px; border-right: 1px solid #4A4A4A;">

							<p style="margin-top:8px;"><strong>Fórum de Combate à Corrupção de Sergipe</strong></p>

							<div class="line" style="margin: 30px 0;"></div>


						</div>

						<!--<div class="row">
							<div class="col-md-4 clearfix">
								<a href="https://www.twitter.com/foccosergipe" target="_blank" class="social-icon si-dark si-colored si-twitter nobottommargin"
									style="margin-right: 10px;">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>
								<a target="_blank" href="https://www.instagram.com/foccosergipe/"><small style="display: block; margin-top: 3px;"><strong>Siga-nos </strong><br>no Twitter</small></a>
							</div>
							<div class="col-md-4 clearfix">
								<a href="https://www.instagram.com/foccosergipe/" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
									style="margin-right: 10px;">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a>
								<a target="_blank" href="https://www.instagram.com/foccosergipe/"><small style="display: block; margin-top: 3px;"><strong>Siga-nos </strong><br>no Instagram</small></a>
							</div>
							<div class="col-md-4 clearfix">
								<a href="http://www.facebook.com/foccosergipe" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
									style="margin-right: 10px;">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>
								<a target="_blank" href="http://www.facebook.com/foccosergipe"><small style="display: block; margin-top: 3px;"><strong>Curta a página </strong><br>no Facebook</small></a>
							</div>
						</div>-->
					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px; margin-top:-30px;">

							<div class="row" style="padding:20px;">
								<div class="col-md-6"></div>
								<div class="col-md-6 clearfix">
									<a href="https://www.instagram.com/foccosergipe/" target="_blank" class="social-icon si-dark si-colored si-twitter nobottommargin"
										style="margin-right: 10px;">
										<i class="icon-twitter"></i>
										<i class="icon-twitter"></i>
									</a>
									<a target="_blank" href="https://www.instagram.com/foccosergipe/"><small style="display: block; margin-top: 3px;"><strong>Siga-nos </strong><br>no Twitter</small></a>
								</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-md-6"></div>
								<div class="col-md-6 clearfix">
									<a href="https://www.instagram.com/foccosergipe/" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin"
										style="margin-right: 10px;">
										<i class="icon-instagram"></i>
										<i class="icon-instagram"></i>
									</a>
									<a target="_blank" href="https://www.instagram.com/foccosergipe/"><small style="display: block; margin-top: 3px;"><strong>Siga-nos </strong><br>no Instagram</small></a>
								</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-md-6"></div>
								<div class="col-md-6 clearfix">
									<a href="http://www.facebook.com/foccosergipe" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
										style="margin-right: 10px;">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a target="_blank" href="http://www.facebook.com/foccosergipe"><small style="display: block; margin-top: 3px;"><strong>Curta a página </strong><br>no Facebook</small></a>
								</div>

							</div>

						</div>

					</div>

				</div>
				<!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						<div class="copyrights-menu copyright-links clearfix">
							<a href="/admin">Acessar Sistema</a>
						</div>
						Copyrights &copy; 2016 Todos os Direitos Reservados.
					</div>

					<div class="col_half col_last tright">
					</div>

				</div>

			</div>
			<!-- #copyrights end -->

		</footer>
		<!-- #footer end -->

	</div>
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
	<script src="{{asset('/js/marked.min.js')}}"></script> @yield('js')

</body>

</html>