@extends('layouts.site') @section('content')

<section id="content">

    <div class="content-wrap" style="padding-top:40px;">

        <div class="container clearfix">

            <div class="single-post nobottommargin">

                <div class="entry clearfix">

                    <div class="entry-title">
                        <h2>{{$noticia->titulo}}</h2>
                    </div>

                    <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> {{$noticia->created_at->format('d/m/Y')}}</li>
                    </ul>

                    <div style="text-align:center;" class="entry-image">
                        <a href="#"><img style="margin:auto;max-width:70%;height:auto;border: 1px solid #ddd;border-radius: 4px;padding: 5px;" src="{{asset('uploads/'.$noticia->image)}}" alt="Blog Single"></a>
                        <i>{{$noticia->legenda}}</i>
                    </div>

                    <div class="entry-content notopmargin" style="font-size:16px;text-align:justify" id="postcontent">{{$noticia->conteudo}}</div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection @section('js')
<script>
    (function() {
        var content = document.getElementById('postcontent').innerHTML;
        document.getElementById('postcontent').innerHTML = marked(content);            
    })();
  </script> @endsection