@extends('layouts.site') @section('content')

<section id="content">


    <div class="content-wrap" style="padding-top:40px;">

        <div class="container clearfix">

            <!-- Postcontent
					============================================= -->
            <div class="postcontent nobottommargin">

                <h3>Fale Conosco</h3>

                <div class="contact-widget" data-alert-type="inline">

                    <div class="contact-form-result"></div>

                    <form class="nobottommargin" id="template-contactform" name="template-contactform" action="/contato" method="post">

                    {{ csrf_field() }}

                        <div class="form-process"></div>

                        <div class="col_full">
                            <p>Preencha o formulário abaixo</p>
                        </div>                        

                        <div class="col_one_third">
                            <label for="nome">Nome <small>*</small></label>
                            <input type="text" id="nome" name="nome" value="" class="sm-form-control required"
                            />
                        </div>

                        <div class="col_one_third">
                            <label for="email">Email <small>*</small></label>
                            <input type="email" id="email" name="email" value="" class="required email sm-form-control"
                            />
                        </div>

                        <div class="col_one_third col_last">
                            <label for="municipio">Município <small>*</small></label>
                            <input type="text" id="municipio" name="municipio" value="" class="required sm-form-control" />
                        </div>

                        <div class="clear"></div>

                        <div class="col_full">
                            <label for="assunto">Assunto <small>*</small></label>
                            <input type="text" id="assunto" name="assunto" value="" class="required sm-form-control"
                            />
                        </div>

                        <div class="clear"></div>

                        <div class="col_full">
                            <label for="menssagem">Menssagem <small>*</small></label>
                            <textarea class="required sm-form-control" id="menssagem" name="menssagem" rows="6"
                                cols="30"></textarea>
                        </div>

                        <div class="col_full hidden">
                            <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control"
                            />
                        </div>

                        <div class="col_full">
                            <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" value="submit">Enviar</button>
                        </div>

                    </form>

                </div>

            </div>
            <!-- .postcontent end -->

            <!-- Sidebar
					============================================= -->
            <div class="sidebar col_last nobottommargin">
            </div>
            <!-- .sidebar end -->

        </div>

    </div>

    </section @endsection