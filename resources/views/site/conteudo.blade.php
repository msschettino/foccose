@extends('layouts.site') @section('content')

<section id="content">
    <div class="content-wrap" style="padding-top:40px;">
        <div class="container clearfix">

            <div class="heading-block left">
                
            </div>
            <input type="hidden" id="conteudoText" value="{{$conteudo}}">
            <div class="postcontent nobottommargin" id="conteudo" style="text-align:justify;">
            </div>            
        </div>
    </div>
</section>

@endsection @section('js')
<script>
    document.getElementById('conteudo').innerHTML =
      marked(document.getElementById('conteudoText').value);
  </script> @endsection