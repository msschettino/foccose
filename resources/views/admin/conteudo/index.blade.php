@extends('layouts.app') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form method="POST" action="/admin/conteudo">
            {{ csrf_field() }}
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#sobre" aria-controls="home" role="tab" data-toggle="tab">Sobre</a></li>
                    <li role="presentation"><a href="#fiscalizacao" aria-controls="profile" role="tab" data-toggle="tab">Fiscalização</a></li>
                    <li role="presentation"><a href="#denuncie" aria-controls="messages" role="tab" data-toggle="tab">Denuncie</a></li>                    
                    <li role="presentation"><a href="#documentos" aria-controls="messages" role="tab" data-toggle="tab">Documentos</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="sobre">
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="sobre" data-provide="markdown" rows="16" style="width:100%;" data-resize="both" data-iconlibrary="fa">{{$conteudo['sobre']->conteudo}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="fiscalizacao">
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="fiscalizacao" data-provide="markdown" rows="16" style="width:100%;" data-resize="both" data-iconlibrary="fa">{{$conteudo['fiscalizacao']->conteudo}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="denuncie">
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="denuncie" data-provide="markdown" rows="16" style="width:100%;" data-resize="both" data-iconlibrary="fa">{{$conteudo['denuncie']->conteudo}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="documentos">
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="documentos" data-provide="markdown" rows="16" style="width:100%;" data-resize="both" data-iconlibrary="fa">{{$conteudo['documentos']->conteudo}}</textarea>
                            </div>
                        </div>
                    </div>                    
                    <div class="row" style="margin:5px;">
                        <div class="col-md-12 text-center">
                            <input type="submit" class="btn btn-success btn-lg" value="Salvar">
                        </div>
                    </div>
                </div>

            </div>
            </form>
        </div>
    </div>
</div>
@endsection