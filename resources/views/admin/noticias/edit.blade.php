@extends('layouts.app') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/admin/noticias/'.$noticia->id) }}">
                {{ method_field('PUT') }} {{ csrf_field() }}

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="titulo" class="col-md-2 control-label">Título</label>

                    <div class="col-md-8">
                        <input id="titulo" type="text" class="form-control" name="titulo" value="{{$noticia->titulo}}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="titulo" class="col-md-2 control-label">Introdução</label>

                    <div class="col-md-8">
                        <input id="intro" type="text" class="form-control" name="intro" value="{{ $noticia->intro }}" required autofocus>
                    </div>
                </div>                

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image" class="col-md-2 control-label">Substituir capa</label>

                    <div class="col-md-8">
                        <input id="image" type="file" class="form-control" name="image">
                    </div>
                </div>

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="legenda" class="col-md-2 control-label">Legenda</label>

                    <div class="col-md-8">
                        <input id="titulo" type="text" class="form-control" name="legenda" value="{{ $noticia->legenda }}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('conteudo') ? ' has-error' : '' }}">
                    <label for="conteudo" class="col-md-2 control-label">Conteúdo</label>

                    <div class="col-md-8">
                        <textarea name="conteudo" data-provide="markdown" rows="8" style="width:100%;" data-resize="both" data-iconlibrary="fa">{{$noticia->conteudo}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <a class="btn btn-danger" href="#" data-toggle="modal" data-target=".destroy-noticia">Exlcuir</a>
                        <button type="submit" class="btn btn-primary">Salvar</button>                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade destroy-noticia" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Excluir notícia</h4>
      </div>
      <div class="modal-body">
        Essa ação não poderá ser desfeita. Deseja continuar?
      </div>
      <div class="modal-footer">
      <form method="POST" action="/admin/noticias/{{$noticia->id}}">
        {{ method_field('DELETE') }}
        {{csrf_field()}}
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Excluir</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
