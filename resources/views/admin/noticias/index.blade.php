@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="/admin/noticias/nova" class="btn btn-success">Nova</a>
        </div>
    </div>

    @if (isset($noticias) && count($noticias) > 0)
    @foreach($noticias as $noticia)
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="/admin/noticias/{{ $noticia->id }}"><h2><small>{{ $noticia->created_at->format('d/m/Y') }}</small><br>{{ $noticia->titulo }}</h2></a>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            {{ $noticias->links() }}
        </div>
    </div>    
    @endif    
</div>    
@endsection