@extends('layouts.app') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/admin/noticias/nova') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="titulo" class="col-md-2 control-label">Título</label>

                    <div class="col-md-8">
                        <input id="titulo" type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="intro" class="col-md-2 control-label">Introdução</label>

                    <div class="col-md-8">
                        <input id="titulo" type="text" class="form-control" name="intro" value="{{ old('intro') }}" required autofocus>
                    </div>
                </div>                

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image" class="col-md-2 control-label">Capa</label>

                    <div class="col-md-8">
                        <input id="image" type="file" class="form-control" name="image" required>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                    <label for="legenda" class="col-md-2 control-label">Legenda</label>

                    <div class="col-md-8">
                        <input id="titulo" type="text" class="form-control" name="legenda" value="{{ old('legenda') }}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('conteudo') ? ' has-error' : '' }}">
                    <label for="conteudo" class="col-md-2 control-label">Conteúdo</label>

                    <div class="col-md-8">
                        <textarea name="conteudo" data-provide="markdown" rows="8" style="width:100%;" data-fullscreen="false" data-resize="both" data-iconlibrary="fa">{{old('conteudo')}}</textarea>
                    </div>
                </div>                

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                                    Salvar
                                </button>

                        <!--<a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection