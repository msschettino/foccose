@extends('layouts.app') @section('content')

<div class="container"> 
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>{{ (isset($msg)) ? $msg : 'Operação realizada com sucesso!' }}</h1> <br>
            <a href="{{ (isset($return_url)) ? $return_url : '/admin' }}" class="btn btn-large btn-success">Voltar</a>
        </div>
    </div>
</div>

@endsection