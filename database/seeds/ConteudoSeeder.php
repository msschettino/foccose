<?php

use Illuminate\Database\Seeder;

class ConteudoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conteudos')->insert([
            'id' => 'sobre',
            'conteudo' => 'conteúdo sobre'
        ]);

        DB::table('conteudos')->insert([
            'id' => 'fiscalizacao',
            'conteudo' => 'conteúdo fiscalizacao'
        ]);        

        DB::table('conteudos')->insert([
            'id' => 'denuncie',
            'conteudo' => 'conteúdo denuncie'
        ]);

        DB::table('conteudos')->insert([
            'id' => 'documentos',
            'conteudo' => 'conteúdo documentos'
        ]);                
    }
}
