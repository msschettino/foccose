<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Noticia;
use App\Http\Controllers\ImageRepository;

class NoticiaController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::orderBy('created_at', 'DESC')->paginate(5);

        return view('admin.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Noticia\CreateRequest $request)
    {
        $noticia = new Noticia($request->except(['_token', 'image']));
        $r = new ImageRepository();
        $n = $r->upload($request->file('image'));
        $noticia->image = $n;
        
        $noticia->save();
        return view('admin.util.success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $noticia = Noticia::findOrFail($id);
            return view('admin.noticias.edit', compact('noticia'));
        } catch (Exception $e) {
            return redirect('/admin/noticias');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\Noticia\UpdateRequest $request, $id)
    {
        try {
            $noticia = Noticia::findOrFail($id);
            $noticia->titulo = $request->input('titulo');
            $noticia->conteudo = $request->input('conteudo');
            $noticia->intro = $request->input('intro');
            $noticia->legenda = $request->input('legenda');

            if ($request->file('image')) {
                \File::delete(public_path() . '/uploads/' .$noticia->image);
                $r = new ImageRepository();
                $n = $r->upload($request->file('image'));
                $noticia->image = $n;                
            }

            $noticia->save();
            return view('admin.util.success', compact('noticia'));
        } catch (Exception $e) {
            return redirect('/admin/noticias');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $noticia = Noticia::findOrFail($id);
            \File::delete(public_path() . '/uploads/' .$noticia->image);
            $noticia->delete();
            return redirect('/admin/noticias');
        } catch (Exception $e) {
            return redirect('/admin/noticias');
        }
    }
}
