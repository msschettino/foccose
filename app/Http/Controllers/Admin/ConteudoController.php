<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Conteudo;

class ConteudoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $conteudo = [];

        $conteudo['sobre'] = Conteudo::find('sobre');
        $conteudo['fiscalizacao'] = Conteudo::find('fiscalizacao');
        $conteudo['denuncie'] = Conteudo::find('denuncie');
        $conteudo['documentos'] = Conteudo::find('documentos');        

        return view('admin.conteudo.index', compact('conteudo'));
    }    

    public function store(Requests\ConteudoRequest $request) {
        $sobre = Conteudo::find('sobre');
        $sobre->conteudo = $request->sobre;
        $sobre->save();

        $fiscalizacao = Conteudo::find('fiscalizacao');
        $fiscalizacao->conteudo = $request->fiscalizacao;
        $fiscalizacao->save();

        $denuncie = Conteudo::find('denuncie');
        $denuncie->conteudo = $request->denuncie;
        $denuncie->save();

        $documentos = Conteudo::find('documentos');
        $documentos->conteudo = $request->documentos;
        $documentos->save();                        

        return view('admin.util.success');
    }
}
