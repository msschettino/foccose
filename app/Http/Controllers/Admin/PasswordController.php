<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.passwords.reset');
    }

    public function update(Requests\PasswordRequest $request) {
        $pass = $request->input('password');
        $conf = $request->input('password_confirmation');
        $curr = $request->input('password_current');
        if ($pass != $conf) {
            return view('auth.passwords.reset')->withErrors('Senhas não conferem');
        }      

        $id = Auth::id();
        $user = User::find($id);
        if (! \Hash::check($curr, $user->password)){
            return view('auth.passwords.reset')->withErrors('Senhas não conferem');
        }

        $user->password = bcrypt($pass);
        $user->save();

        return view('admin.util.success');
    }
}
