<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;

class ContatoController extends Controller
{

    public function index() {
        return view('site.contato');
    }

    public function create(Requests\ContatoRequest $request) {
        try {
            $data = $request->except(['_token']); 
            Mail::send('mail_basic', $data, function($message) {
                $message->from('mail@codeship.com.br', 'naoresponda');
                $message->to('schettino2@gmail.com', 'FOCCO')->subject('Formulário de Contato :: Focco/SE');
            });
            $response = [
                'alert' => 'success',
                'message' => 'Menssagem enviada com sucesso',
            ];
        } catch (Exception $e) {
            $response = [];
        }
        return response()->json($response, 200);
    }
}
