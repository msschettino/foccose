<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Conteudo;

class ConteudoController extends Controller
{
    public function index($section) {
        try {
            $page = Conteudo::findOrFail($section);
            
            $titles = [
                'sobre' => 'sobre',
                'fiscalizacao' => 'fiscalização',
                'denuncie' => 'denuncie',
                'documentos' => 'documentos'
            ];

            $title = $titles[$section];
            $conteudo = $page->conteudo;

            return view('site.conteudo', compact('conteudo', 'title'));
        } catch (Exception $e) {
            return redirect('/');
        }
    }
}
