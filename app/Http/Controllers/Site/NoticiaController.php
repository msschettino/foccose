<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Noticia;

class NoticiaController extends Controller
{
    public function index() {
        $noticias = Noticia::orderBy('created_at', 'DESC')->paginate(5);
        return view('welcome', compact('noticias'));
    }

    public function show($id) {
        try {
            $noticia = Noticia::findOrFail($id);
            return view('site.noticia', compact('noticia'));
        } catch(Exception $e) {
            return redirect('/');
        }
    }
}
