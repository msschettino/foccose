<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;

class ImageRepository {

    public $upload_dir;

    public function __construct() {
        $this->upload_dir = public_path() . '/uploads/';
    }

    /**
     * Store a picture
     *
     * @param  FormData file  $image
     * @return string -> image name without extension
     */
    public function upload($image) {
        $n = $this->createUniqueFilename() . '.jpg';

        $dir = public_path() . '/uploads/';

        $m = new ImageManager();
        $m->make($image)
          ->encode('jpg')
          ->save($dir .  $n, 70);

        return $n;
    }

    protected function createUniqueFilename()
    {
        $imageToken = substr(sha1(mt_rand()), 0, 5);
        return time() . '-' . $imageToken;
    }    
}