<?php

namespace App\Http\Requests\Noticia;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required',
            'conteudo' => 'required',
            'intro' => 'required',
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'legenda' => ''
        ];
    }
}
