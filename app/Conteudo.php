<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conteudo extends Model
{
    public $incrementing = false;

    protected $fillable = ['id', 'conteudo'];
}
