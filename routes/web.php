<?php

use Illuminate\Support\Facades\Response;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Site\NoticiaController@index');
Route::get('/noticia/{id}', 'Site\NoticiaController@show');
Route::get('/conteudo/{section}', 'Site\ConteudoController@index');
Route::get('/contato', 'Site\ContatoController@index');
Route::post('/contato', 'Site\ContatoController@create');

Auth::routes();
Route::get('/auth/password', 'Admin\PasswordController@index');
Route::post('/auth/password', 'Admin\PasswordController@update');

Route::get('/admin', 'Admin\NoticiaController@index');
Route::get('/admin/noticias', 'Admin\NoticiaController@index');
Route::get('/admin/noticias/nova', 'Admin\NoticiaController@create');
Route::post('/admin/noticias/nova', 'Admin\NoticiaController@store');
Route::get('/admin/noticias/{id}', 'Admin\NoticiaController@edit');
Route::put('/admin/noticias/{id}', 'Admin\NoticiaController@update');
Route::delete('/admin/noticias/{id}', 'Admin\NoticiaController@destroy');

Route::get('/admin/conteudo', 'Admin\ConteudoController@index');
Route::post('/admin/conteudo', 'Admin\ConteudoController@store');

